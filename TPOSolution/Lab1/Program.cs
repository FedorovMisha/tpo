﻿using System;
using System.IO;
using System.Linq;

namespace Lab1
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var filePath = @"./test.txt";

            Console.WriteLine(MinStringLength(filePath));
        }

        public static bool IsPositiveMatrix(int[,] matrix)
        {
            if (matrix != null && matrix.Length != 3 * 3)
                throw new Exception();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (matrix[i, j] < 0)
                        return false;
                }
            }
            
            return true;
        }

        public static string ProcessString(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
                return inputString;
            
            if (!inputString.Contains("#"))
                return inputString;

            var index = inputString.IndexOf('#');

            if (index == inputString.Length - 1)
                return inputString;

            var firstPath = inputString.Substring(0, index + 1);
            return firstPath + new string('@', inputString.Length - index - 1);
        }

        public static int MinStringLength(string filePath)
        {
            int minLength = int.MaxValue;
            using var sr = new StreamReader(filePath);
            if (sr.EndOfStream) return 0;
            while (!sr.EndOfStream)
            {
                var str = sr.ReadLine();
                if (minLength > str.Length)
                {
                    minLength = str.Length;
                }
            }

            return minLength;
        }
    }
}